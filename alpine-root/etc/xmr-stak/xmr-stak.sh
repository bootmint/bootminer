#!/bin/sh

set -u

# Install hwloc
if [ -f /home/"${miner_user}"/hwloc-1.11.8.tar.gz ]; then
	/bin/su "${miner_user}" -c "tar zxf /home/${miner_user}/hwloc-1.11.8.tar.gz -C /home/${miner_user}/"
else
	echo "Installation failed. Hwloc archive is not available."
	exit 1
fi

cd /home/"${miner_user}"/hwloc-1.11.8/
/bin/su "${miner_user}" -c "./configure"
/bin/su "${miner_user}" -c "make"
make install
cd ..

# Install xmr-stak
if [ -f /home/"${miner_user}"/xmr-stak-"${xmr_stak_release}".tar.gz ]; then
	/bin/su "${miner_user}" -c "tar zxf /home/${miner_user}/xmr-stak-${xmr_stak_release}.tar.gz -C /home/${miner_user}/"
else
	echo "Installation failed. Xmr-stak archive is not available."
	exit 1
fi

sed -i -e "s/fDevDonationLevel = 2.0/fDevDonationLevel = ${donate_percent_xmr_stak}/" "${xmr_stak_dir}"/xmrstak/donate-level.hpp
/bin/su "${miner_user}" -c "mkdir ${xmr_stak_dir}/build"
cd "${xmr_stak_dir}"/build
#/bin/su "${miner_user}" -c "cmake .. -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF -DMICROHTTPD_ENABLE=OFF -DHWLOC_ENABLE=OFF"
/bin/su "${miner_user}" -c "cmake .. -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF -DMICROHTTPD_ENABLE=OFF"
/bin/su "${miner_user}" -c "make"
#make install

# Copy xmr-stak binary
cp "${xmr_stak_dir}"/build/bin/xmr-stak /usr/local/bin/
chown root:root /usr/local/bin/xmr-stak
chmod 755 /usr/local/bin/xmr-stak

# Configure username for proxy client
# TODO: Add network or hardware information
if [ "${system_config}" = "client" ]; then
	mining_pool1_username="$(date +%b_%d_%T)"
	mining_pool2_username="${mining_pool1_username}"
	mining_pool3_username="${mining_pool1_username}"
fi

# Configure and start xmr-stak
cd "${config_file_dir}"
/bin/su "${miner_user}" -c \
	"/usr/local/bin/xmr-stak \
	>> ${config_file_dir}/log.txt 2>&1 \
	<<-__EOF__
	${mining_currency}
	${mining_pool1}
	${mining_pool1_username}
	${mining_pool1_password}
	${mining_pool1_rigid}
	${mining_pool1_tls}
	${mining_pool1_nicehash}
	y
	${mining_pool1_weight}
	${mining_pool2}
	${mining_pool2_username}
	${mining_pool2_password}
	${mining_pool2_rigid}
	${mining_pool2_tls}
	${mining_pool2_nicehash}
	${mining_pool2_weight}
	y
	${mining_pool3}
	${mining_pool3_username}
	${mining_pool3_password}
	${mining_pool3_rigid}
	${mining_pool3_tls}
	${mining_pool3_nicehash}
	${mining_pool3_weight}
	n
	__EOF__" &

sleep 1

# Restart xmr-stak
# TODO: Connection problem between xmr-stak and haproxy when using TLS
# and running for the first time. Look into this problem.
if [ "${system_config}" = "client" ]; then
	pkill /usr/local/bin/xmr-stak
	sleep 10
	if pgrep /usr/local/bin/xmr-stak >> /dev/null; then
		pkill -9 /usr/local/bin/xmr-stak
		sleep 10
	fi

	# Start xmr-stak
	/bin/su "${miner_user}" -c "/usr/local/bin/xmr-stak >> ${config_file_dir}/log.txt 2>&1" &
fi
	

if [ "${system_config}" = "normal" ]; then
	cp "${config_file}" "${config_file_dir}"/.config.txt
	cp "${pool_file}" "${config_file_dir}"/.pools.txt
	cp "${cpu_file}" "${config_file_dir}"/.cpu.txt

	sed -i -e "s/${wallet_address}/${donate_address}/" "${config_file_dir}"/.pools.txt

	chown "${miner_user}":"${miner_user}" "${config_file_dir}"/.*.txt
	chmod 640 "${config_file_dir}"/.*.txt
fi

# Second wallet
: "${wallet_address_second:=false}"
if [ "${wallet_address_second}" != "false" ] && [ "${system_config}" = "normal" ]; then
	cp "${pool_file}" "${config_file_dir}"/pools-second-wallet.txt
	sed -i -e "s/${wallet_address}/${wallet_address_second}/" "${config_file_dir}"/pools-second-wallet.txt
fi

# Create donate.sh script
if [ "${system_config}" = "normal" ]; then 
	cat >> "${config_file_dir}"/donate.sh <<-"__EOF__"
		#!/bin/sh
	
		# Use a value between 0-50. 0.2 is 0.2%, not 2.0%. Set to 0 to disable.
		donate_percent="donation_percent"
	
		# Configure a wallet address for donations
		donate_address="donation_address"
	
		# Mining rig id
		rig_id=""
	
		if expr "${donate_percent}" : '[[:digit:]]\+$' > /dev/null; then
		        if [ "${donate_percent}" -eq 0 ]; then
		                echo "Mining is disabled. Increase percentage to enable."
		                exit 0
		        elif [ "${donate_percent}" -ge 50 ]; then
		                echo "Mining percentage equal or above 50. Will set it slightly below 50."
		                donate_percent="49.725"
		        fi
		fi
	
		# Seconds to mine each day
		# Double the number to compensate for mining with two mining processes at the same time
		mining_seconds=$(dc 86400 100 div ${donate_percent} mul 2 mul p)
	
		# Random sleep
		#sleep $(shuf -i 3600-7200 -n 1 )
	
		# Run a single instance of donation miner
		pgrep -f "/usr/local/bin/xmr-stak --config .config.txt --poolconf .pools.txt --cpu .cpu.txt" && exit 0
		pgrep -f "/usr/local/bin/xmr-stak --tls-url gulf.moneroocean.stream:443 --user ${donate_address} --rigid ${rig_id} --pass x --currency monero" && exit 0
	
		# Use config files or manual pool
		if [ -f .config.txt ] && [ -f .pools.txt ] && [ -f .cpu.txt ] && \
		   grep -q '^"currency" : "monero"' .pools.txt; then
		        /usr/local/bin/xmr-stak --config .config.txt --poolconf .pools.txt --cpu .cpu.txt &
		        sleep 5
		        sleep "${mining_seconds}"
		        pkill -f "/usr/local/bin/xmr-stak --config .config.txt --poolconf .pools.txt --cpu .cpu.txt"
		        sleep 10
		        pkill -9 -f "/usr/local/bin/xmr-stak --config .config.txt --poolconf .pools.txt --cpu .cpu.txt"
		else
		        # Don't overwrite config files
		        mkdir -p ~/.xmr-stak
		        cd ~/.xmr-stak
		        /usr/local/bin/xmr-stak --tls-url gulf.moneroocean.stream:443 --user "${donate_address}" --rigid "${rig_id}" --pass x --currency monero &
		        sleep 5
		        sleep "${mining_seconds}"
		        pkill -f "/usr/local/bin/xmr-stak --tls-url gulf.moneroocean.stream:443 --user ${donate_address} --rigid ${rig_id} --pass x --currency monero"
		        sleep 10
		        pkill -9 -f "/usr/local/bin/xmr-stak --tls-url gulf.moneroocean.stream:443 --user ${donate_address} --rigid ${rig_id} --pass x --currency monero"
		fi
		__EOF__
fi

if [ "${system_config}" = "normal" ]; then
	sed -i -e "s/donation_address/${donate_address}/" "${config_file_dir}"/donate.sh
	sed -i -e "s/donation_percent/${donate_percent}/" "${config_file_dir}"/donate.sh

	chown "${miner_user}":"${miner_user}" "${config_file_dir}"/donate.sh
	chmod 750 "${config_file_dir}"/donate.sh
fi

if [ "${wallet_address_second}" = "false" ] && [ "${system_config}" = "normal" ]; then
  /bin/su "${miner_user}" -c "sleep $(shuf -i 3600-7200 -n 1) && /home/${miner_user}/donate.sh >> ${config_file_dir}/log.txt 2>&1" &
fi

# Create second-wallet.sh script
# TODO: merge with donate.sh
if [ "${wallet_address_second}" != "false" ] && [ "${system_config}" = "normal" ]; then
	cat >> "${config_file_dir}"/second-wallet.sh <<-"__EOF__"
		#!/bin/sh

		# Use a value between 0-50. 0.2 is 0.2%, not 2.0%. Set to 0 to disable.
		donate_percent="donation_percent"

		# Configure a wallet address
		donate_address="donation_address"

		# Configure cryptonote currency
		mining_currency="cryptonote_currency"

		# Manual backup pool
		backup_pool="mining_pool"

		# Mining rig id
		rig_id=""

		if expr "${donate_percent}" : '[[:digit:]]\+$' > /dev/null; then
		        if [ "${donate_percent}" -eq 0 ]; then
		                /home/mining_user/donate.sh &
		                echo "Mining is disabled. Increase percentage to enable."
		                exit 0
		        elif [ "${donate_percent}" -ge 50 ]; then
		                echo "Mining percentage equal or above 50. Will set it slightly below 50."
		                donate_percent="49.725"
		        fi
		fi

		# Seconds to mine each day
		# Double the number to compensate for mining with two mining processes at the same time
		mining_seconds=$(dc 86400 100 div ${donate_percent} mul 2 mul p)

		# Run a single instance of second wallet miner
		pgrep -f "/usr/local/bin/xmr-stak --config config.txt --poolconf pools-second-wallet.txt --cpu cpu.txt" && exit 0
		pgrep -f "/usr/local/bin/xmr-stak --tls-url ${backup_pool} --user ${donate_address} --rigid ${rig_id} --pass x --currency ${mining_currency}" && exit 0

		# Use config files or manual pool
		if [ -f config.txt ] && [ -f pools-second-wallet.txt ] && [ -f cpu.txt ]; then
		        /usr/local/bin/xmr-stak --config config.txt --poolconf pools-second-wallet.txt --cpu cpu.txt &
		        sleep 5
		        sleep "${mining_seconds}"
		        pkill -f "/usr/local/bin/xmr-stak --config config.txt --poolconf pools-second-wallet.txt --cpu cpu.txt"
		        sleep 10
		        pkill -9 -f "/usr/local/bin/xmr-stak --config config.txt --poolconf pools-second-wallet.txt --cpu cpu.txt"
		else
		        # Don't overwrite config files
		        mkdir -p ~/xmr-stak-second-wallet
		        cd ~/xmr-stak-second-wallet
		        /usr/local/bin/xmr-stak --tls-url "${backup_pool}" --user "${donate_address}" --rigid "${rig_id}" --pass x --currency "${mining_currency}" &
		        sleep 5
		        sleep "${mining_seconds}"
		        pkill -f "/usr/local/bin/xmr-stak --tls-url ${backup_pool} --user ${donate_address} --rigid ${rig_id} --pass x --currency ${mining_currency}"
		        sleep 10
		        pkill -9 -f "/usr/local/bin/xmr-stak --tls-url ${backup_pool} --user ${donate_address} --rigid ${rig_id} --pass x --currency ${mining_currency}"
		fi

		# Run donate.sh
		/home/mining_user/donate.sh &
		__EOF__

	sed -i -e "s/donation_address/${wallet_address_second}/" "${config_file_dir}"/second-wallet.sh
	sed -i -e "s/donation_percent/${donate_percent_second}/" "${config_file_dir}"/second-wallet.sh
	sed -i -e "s/cryptonote_currency/${mining_currency}/" "${config_file_dir}"/second-wallet.sh
	sed -i -e "s/mining_pool/${mining_pool1}/" "${config_file_dir}"/second-wallet.sh
	sed -i -e "s/mining_user/${miner_user}/" "${config_file_dir}"/second-wallet.sh

	chown "${miner_user}":"${miner_user}" "${config_file_dir}"/second-wallet.sh
	chmod 750 "${config_file_dir}"/second-wallet.sh
	/bin/su "${miner_user}" -c "/home/${miner_user}/second-wallet.sh >> ${config_file_dir}/log.txt 2>&1" &
fi

# Configure crontabs
cat >> /etc/crontabs/"${miner_user}" <<-__EOF__
	# Check for xmr-stak every 10 minutes
	*/10	*	*	*	*	pgrep /usr/local/bin/xmr-stak || /usr/local/bin/xmr-stak >> ${config_file_dir}/log.txt 2>&1
	__EOF__

if [ "${wallet_address_second}" != "false" ] && [ "${system_config}" = "normal" ]; then
	cat >> /etc/crontabs/"${miner_user}" <<-__EOF__

		# Run second-wallet.sh every day
		$(shuf -i 0-59 -n 1)	$(shuf -i 0-23 -n 1)	*	*	*	${config_file_dir}/second-wallet.sh >> ${config_file_dir}/log.txt 2>&1
		__EOF__
elif [ "${system_config}" = "normal" ]; then
	cat >> /etc/crontabs/"${miner_user}" <<-__EOF__

		# Run donate.sh every day
		$(shuf -i 0-59 -n 1)	$(shuf -i 0-23 -n 1)	*	*	*	${config_file_dir}/donate.sh >> ${config_file_dir}/log.txt 2>&1
		__EOF__
fi
