#!/bin/sh
#
# BootMiner custom iso generator.
# Don't change any values unless you know what you are doing.
#
# License:
# MIT
#
# BootMiner Monero donation address:
# 45YAKAGsns8VZKYuWyzctzSCgd3k1VUqLFre7fKRRvxiaJEftCsQNMYVoddDPjPxB4QSY5BSsSoVB1BomzVopfKgEVT6Cyj

set -e
set -u

# Set mining address.
# Best to leave this empty and use the selection menu to set a custom address.
mining_address=""

# Currency to mine. Defaults to Monero.
# See xmr-stak for all available currencies.
mining_currency="monero"

# Configure mining pools. Requires ip/hostname:port. Must support TLS.
mining_pool1="gulf.moneroocean.stream:443"
mining_pool2="mine.xmrpool.net:443"
mining_pool3="xmrpool.eu:9999"

# Configure mining proxy servers. Requires ip/hostname:port. Must support TLS.
mining_proxy1="192.168.1.10:8080"
mining_proxy2="${mining_proxy1}"
mining_proxy3="${mining_proxy1}"

# Encrypt apkovl archive. You need to enter this password at boot.
boot_encryption_password=""
boot_encryption_cipher="aes-256-cbc"

# Date for reproducible iso
export SOURCE_DATE_EPOCH="1543989299"

# Check software dependencies
if ! which git wget bsdtar xorriso sha256sum sed >/dev/null; then
	echo "Please install all software dependencies"
	exit 1
fi

# Set wallet address
# Other donation options:
# - Monero research lab?
# - Monero outreach?
# - Monero bug bounty?
# - Kovri?
menu_opt=""
while [ -z "${menu_opt}" ] && [ -z "${mining_address}" ]; do
	echo "This menu will configure a wallet address for the BootMiner iso."
	echo "You can select one of the predefined options or configure a custom address."
	echo "Please consider to donate to one of the predefined Monero wallets."
	echo ""
	echo "1. MONERO DEVELOPMENT FUND (GETMONERO.ORG)"
	echo "2. MONEROMOOO (IMPORTANT DEV)"
	echo "3. MONERO FORUM FUNDING SYSTEM (FFS)"
	echo "4. MONERUJO (ANDROID WALLET)"
	echo "5. CAKE WALLET (IOS WALLET)"
	echo "6. BOOTMINT (BOOTMINER CREATORS)"
	echo "7. CUSTOM & SECOND WALLET ADDRESS"
	echo "8. MINING PROXY SERVER (ADVANCED)"
	echo "9. MINING PROXY CLIENT (ADVANCED)"
	echo ""
	echo -n "Please enter your option (1-9): "
	read menu_opt

	case "${menu_opt}" in
		1)	mining_address="44AFFq5kSiGBoZ4NMDwYtN18obc8AemS33DBLWs3H7otXft3XjrpDtQGv7SqSsaBYBb98uNbr2VBBEt7f2wfn3RVGQBEP3A"
			system_config="normal"
			echo "Selected the Monero development fund option. Thank you for supporting Monero development!"
			echo "BootMiner will mine to wallet:"
			echo "${mining_address}"
			break
			;;
		2)	mining_address="4AfUP827TeRZ1cck3tZThgZbRCEwBrpcJTkA1LCiyFVuMH4b5y59bKMZHGb9y58K3gSjWDCBsB4RkGsGDhsmMG5R2qmbLeW"
			system_config="normal"
			echo "Selected the Moneromooo option. Thank you for supporting Monero development!"
			echo "BootMiner will mine to wallet:"
			echo "${mining_address}"
			break
			;;
		3)	mining_address="45ttEikQEZWN1m7VxaVN9rjQkpSdmpGZ82GwUps66neQ1PqbQMno4wMY8F5jiDt2GoHzCtMwa7PDPJUJYb1GYrMP4CwAwNp"
			system_config="normal"
			echo "Selected the Monero FFS option. Thank you for supporting Monero development!"
			echo "BootMiner will mine to wallet:"
			echo "${mining_address}"
			break
			;;
		4)	mining_address="4AdkPJoxn7JCvAby9szgnt93MSEwdnxdhaASxbTBm6x5dCwmsDep2UYN4FhStDn5i11nsJbpU7oj59ahg8gXb1Mg3viqCuk"
			system_config="normal"
			echo "Selected the Monerujo option. Thank you for supporting Monerujo!"
			echo "BootMiner will mine to wallet:"
			echo "${mining_address}"
			break
			;;
		5)	mining_address="43gN49UjHNdXDgkcWHTxceHNjXBxcKsReSNThGwzHVavHeZ4SSxSCPT8EpD5cbwAWqEqFQw12rsyTJbKGbeXo43SVpPXZ2W"
			system_config="normal"
			echo "Selected the Cake Wallet option. Thank you for supporting Cake Wallet!"
			echo "BootMiner will mine to wallet:"
			echo "${mining_address}"
			break
			;;
		6)	mining_address="45YAKAGsns8VZKYuWyzctzSCgd3k1VUqLFre7fKRRvxiaJEftCsQNMYVoddDPjPxB4QSY5BSsSoVB1BomzVopfKgEVT6Cyj"
			system_config="normal"
			echo "Selected the BootMint option. Thank you for supporting BootMint!"
			echo "BootMiner will mine to wallet:"
			echo "${mining_address}"
			break
			;;
		7)	system_config="normal"
			echo "Using a custom address."
			break
			;;
		8)	system_config="server"
			echo "Selected mining proxy server."
			break
			;;
		9)	mining_address="mining"
			system_config="client"
			echo "Selected mining proxy client."
			break
			;;
		*)	echo "Invalid option. Please run the script again."
			exit 1
			;;
	esac
done

# Proxy system checks
if [ "${system_config}" = "client" ] || [ "${system_config}" = "server" ]; then
	echo ""
	if [ -s alpine-root/root/.ssh/authorized_keys ] ||
		[ -s alpine-root/home/bootminer/.ssh/authorized_keys ] ||
		[ -s alpine-root/etc/ssh/sshd_config ]; then
		echo "Info: openssh-server will be installed"
	else
		echo "Info: openssh-server will not be installed"
	fi
	if grep -q "^iface eth0 inet dhcp" alpine-root/etc/setup-alpine.conf; then
		echo "Info: eth0 will use DHCP"
	else
		echo "Info: eth0 will not use DHCP"
	fi
	if [ -s alpine-root/etc/wpa_supplicant/wpa_supplicant.conf ]; then
		echo "Info: wireless networking will be configured"
	else
		echo "Info: wireless networking will not be configured"
	fi
	echo ""
	echo -n "Network config correct (y/n): "
	read menu_proxy

	case "${menu_proxy}" in
		y|yes)	break
			;;
		n|no)	echo "Change the network config in: alpine-root/etc/setup-alpine.conf"
			echo "Configure a static or dhcp address. Set a dns server in case"
			echo "you configure a static ip address."
			exit 1
			;;
		*)	echo "Invalid option. Please run the script again."
			exit 1
			;;
	esac
fi

# Proxy client checks
if [ "${system_config}" = "client" ]; then
	echo ""
	echo "The client will use the following proxy server addresses and ports: "
	echo ""
	echo "${mining_proxy1}"
	echo "${mining_proxy2}"
	echo "${mining_proxy3}"
	echo ""
	echo -n "Proxy server config correct (y/n): "
	read menu_client

	case "${menu_client}" in
		y|yes)	break
			;;
		n|no)	echo "Change the proxy server addresses in: $0"
			echo "You need to change: mining_proxy1, mining_proxy2 and mining_proxy3"
			echo "They can all point to the same address."
			echo ""
			echo "Example:"
			echo 'mining_proxy1="192.168.1.10:8080"'
			echo 'mining_proxy2="www.example.com:8080"'
			echo 'mining_proxy3="www.example.com:10443"'
			exit 1
			;;
		*)	echo "Invalid option. Please run the script again."
			exit 1
			;;
	esac
fi

# Basic cryptonote address validation
validate_input () {
	if [ "${#1}" -ne 95 ]; then
		echo "Invalid wallet address. Wrong length."
		echo "Please run the script again."
		exit 1
	else
		if expr "${1}" : '^[[:alnum:]]\+$' > /dev/null; then
			echo "BootMiner will mine to wallet:"
			echo "${1}"
		else
			echo "Invalid wallet address. Invalid characters."
			echo "Please run the script again."
			exit 1
		fi
	fi
}

# Input validation mining percent
validate_percent () {
	if expr "${1}" : '^[[:digit:]]\+$' > /dev/null; then
		if [ "${1}" -lt 1 ]; then
			echo "Percentage is too low. Use a number between: 1-50"
			echo "Please run the script again."
			exit 1
		elif [ "${1}" -gt 50 ]; then
			echo "Percentage is too high. Use a number between: 1-50"
			echo "Please run the script again."
			exit 1
		fi
	else
			echo "Invalid percentage. Invalid characters. Use a number between: 1-50"
			echo "A comma ',' or dot '.' is not allowed."
			echo "Please run the script again."
			exit 1
	fi
}

# Set custom address
while [ -z "${mining_address}" ]; do
	echo ""
	echo "A Monero mining address is 95 characters long and starts with a 4."
	echo "Example Monero mining address of the Monero development fund:"
	echo "44AFFq5kSiGBoZ4NMDwYtN18obc8AemS33DBLWs3H7otXft3XjrpDtQGv7SqSsaBYBb98uNbr2VBBEt7f2wfn3RVGQBEP3A"
	echo ""
	echo "Monero sub-addresses start with an 8, you can't mine to those (yet)."
	echo ""
	echo -n "Enter primary wallet address: "
	read mining_address
	validate_input "${mining_address}"

	echo ""
	echo -n "Would you like to mine a (small) portion to a second address, like the Monero Forum Funding System (y/n): "
	read menu_second

	case "${menu_second}" in
		y|yes)	echo ""
			echo "Mining suggestion - Contribute to the development of Monero and donate 1-10% to the Monero FFS:"
			echo "45ttEikQEZWN1m7VxaVN9rjQkpSdmpGZ82GwUps66neQ1PqbQMno4wMY8F5jiDt2GoHzCtMwa7PDPJUJYb1GYrMP4CwAwNp"
			echo ""
			echo -n "Enter the second wallet address: "
			read mining_address_second
			validate_input "${mining_address_second}"
			echo ""
			echo -n "Enter the mining percentage for the second wallet (1-50): "
			read mining_address_second_percent
			validate_percent "${mining_address_second_percent}"
			break
			;;
		n|no)	break
			;;
		*)	echo "Invalid option. Please run the script again."
			exit 1
			;;
	esac
done

# Route mining traffic via Tor
menu_tor=""
while [ -z "${menu_tor}" ]; do
	echo ""
	echo -n "Would you like to route mining traffic via Tor (y/n): "
	read menu_tor

	case "${menu_tor}" in
		y|yes)	mkdir -p alpine-root/etc/tor
			break
			;;
		n|no)	if [ -d alpine-root/etc/tor ]; then
				rm -r alpine-root/etc/tor
			fi
			break
			;;
		*)	echo "Invalid option. Please run the script again."
			exit 1
			;;
	esac
done

# Encrypt apkovl archive
menu_archive=""
while [ -z "${menu_archive}" ] && [ -z "${boot_encryption_password}" ]; do
	echo ""
	echo "It is possible to encrypt sensitive files on the BootMiner image."
	echo "If you enable this option you will need to enter the password at each boot."
	echo -n "Would you like to encrypt sensitive files on the image (y/n): "
	read menu_archive

	case "${menu_archive}" in
		y|yes)	encrypt_archive="true"
			break
			;;
		n|no)	encrypt_archive="false"
			break
			;;
		*)	echo "Invalid option. Please run the script again."
			exit 1
			;;
	esac
done

# Check for existing alpine-root directory or download BootMiner repo
# TODO: support reproducible iso. Download release?
if [ ! -d alpine-root ]; then
	if git clone https://bitbucket.org/bootmint/bootminer.git; then
		cd bootminer
	else
		if [ -d bootminer ]; then
			rm -r bootminer
		fi
		echo "Failed to download BootMiner. Please run the script again."
		exit 1
	fi
fi

# Verify alpine iso
alpine_release="alpine-standard-3.8.1-x86_64"
alpine_checksum () {
	if ! echo "70d231a2dc1366d0a69810b3ea8d4d52a6c23503c4db6db1d2dc9815e2bc90e4  ${alpine_release}.iso" | sha256sum --check --status; then
		if [ -f "${alpine_release}".iso ]; then
			rm "${alpine_release}".iso
		fi
		echo "Checksum error for the Alpine Linux iso. Please run the script again."
		exit 1
	fi
}

# Check or download alpine linux iso
# Use CDN with https. Don't check certificate because of domain mismatch
if [ -f "${alpine_release}".iso ]; then
	alpine_checksum
else
	if wget --no-check-certificate https://dl-cdn.alpinelinux.org/alpine/v3.8/releases/x86_64/"${alpine_release}".iso; then
		alpine_checksum
	else
		if [ -f "${alpine_release}".iso ]; then
			rm "${alpine_release}".iso
		fi
		echo "Failed to download Alpine Linux iso. Please run the script again."
		exit 1
	fi
fi

# Extract alpine iso
if [ -d "${alpine_release}" ]; then
	chmod --recursive 700 "${alpine_release}"
	rm -r "${alpine_release}"
	mkdir "${alpine_release}"
else
	mkdir "${alpine_release}"
fi

if ! bsdtar -xf "${alpine_release}".iso -C "${alpine_release}"; then
	echo "Failed to extract the Alpine Linux iso. Please run the script again."
	exit 1
fi

# Verify xmr-stak
xmr_stak_release="2.7.0"
xmr_stak_checksum () {
	if ! echo "8018cdfe53e02ce63ddc447fe57087eda6eff998ecfec90705de8d33a7178715  alpine-root/home/bootminer/xmr-stak-${xmr_stak_release}.tar.gz" | sha256sum --check --status; then
		if [ -f alpine-root/home/bootminer/xmr-stak-"${xmr_stak_release}".tar.gz ]; then
			rm alpine-root/home/bootminer/xmr-stak-"${xmr_stak_release}".tar.gz
		fi
		echo "Checksum error for xmr-stak. Please run the script again."
		exit 1
	fi
}

# Check or download xmr-stak
if [ "${system_config}" = "normal" ] || [ "${system_config}" = "client" ]; then
	if [ -f alpine-root/home/bootminer/xmr-stak-"${xmr_stak_release}".tar.gz ]; then
		xmr_stak_checksum
	else
		if wget https://github.com/fireice-uk/xmr-stak/archive/"${xmr_stak_release}".tar.gz -O alpine-root/home/bootminer/xmr-stak-"${xmr_stak_release}".tar.gz; then
			xmr_stak_checksum
		else
			if [ -f alpine-root/home/bootminer/xmr-stak-"${xmr_stak_release}".tar.gz ]; then
				rm alpine-root/home/bootminer/xmr-stak-"${xmr_stak_release}".tar.gz
			fi
			echo "Failed to download xmr-stak. Please run the script again."
			exit 1
		fi
	fi
fi

# Verify hwloc
hwloc_release="hwloc-1.11.8"
hwloc_checksum () {
	if ! echo "8af89b1164a330e36d18210360ea9bb305e19f9773d1c882855d261a13054ea8  alpine-root/home/bootminer/${hwloc_release}.tar.gz" | sha256sum --check --status; then
		if [ -f alpine-root/home/bootminer/"${hwloc_release}".tar.gz ]; then
			rm alpine-root/home/bootminer/"${hwloc_release}".tar.gz
		fi
		echo "Checksum error for hwloc. Please run the script again."
		exit 1
	fi
}

# Check or download hwloc
if [ "${system_config}" = "normal" ] || [ "${system_config}" = "client" ]; then
	if [ -f alpine-root/home/bootminer/"${hwloc_release}".tar.gz ]; then
		hwloc_checksum
	else
		if wget https://www.open-mpi.org/software/hwloc/v1.11/downloads/"${hwloc_release}".tar.gz -O alpine-root/home/bootminer/"${hwloc_release}".tar.gz; then
			hwloc_checksum
		else
			if [ -f alpine-root/home/bootminer/"${hwloc_release}".tar.gz ]; then
				rm alpine-root/home/bootminer/"${hwloc_release}".tar.gz
			fi
			echo "Failed to download hwloc. Please run the script again."
			exit 1
		fi
	fi
fi

# Verify xmrig-proxy
xmrig_proxy_release="2.8.1"
xmrig_proxy_checksum () {
	if ! echo "a0c95924810d77d6f83a230052b01564ffe77909e90b366f076179089e151694  alpine-root/home/bootminer/xmrig-proxy-${xmrig_proxy_release}.tar.gz" | sha256sum --check --status; then
		if [ -f alpine-root/home/bootminer/xmrig-proxy-"${xmrig_proxy_release}".tar.gz ]; then
			rm alpine-root/home/bootminer/xmrig-proxy-"${xmrig_proxy_release}".tar.gz
		fi
		echo "Checksum error for xmrig-proxy. Please run the script again."
		exit 1
	fi
}

# Check or download xmrig-proxy
if [ "${system_config}" = "server" ]; then
	if [ -f alpine-root/home/bootminer/xmrig-proxy-"${xmrig_proxy_release}".tar.gz ]; then
		xmrig_proxy_checksum
	else
		if wget https://github.com/xmrig/xmrig-proxy/archive/v"${xmrig_proxy_release}".tar.gz -O alpine-root/home/bootminer/xmrig-proxy-"${xmrig_proxy_release}".tar.gz; then
			xmrig_proxy_checksum
		else
			if [ -f alpine-root/home/bootminer/xmrig-proxy-"${xmrig_proxy_release}".tar.gz ]; then
				rm alpine-root/home/bootminer/xmrig-proxy-"${xmrig_proxy_release}".tar.gz
			fi
			echo "Failed to download xmrig-proxy. Please run the script again."
			exit 1
		fi
	fi
fi

# Modify wallet address
if [ -n "${mining_address}" ]; then
	if [ "${system_config}" = "normal" ]; then
		if ! sed -i -e "s#^export wallet_address=.*\$#export wallet_address=\"${mining_address}\"#" alpine-root/etc/local.d/boot.start; then
			echo "Failed to modify wallet address. Please run the script again."
			exit 1
		fi
	fi
	if [ "${system_config}" = "server" ]; then
		if ! sed -i -e "s#\"user\": \".*\$#\"user\": \"${mining_address}\",#" alpine-root/etc/xmrig-proxy/config.json; then
			echo "Failed to modify wallet address. Please run the script again."
			exit 1
		fi
	fi
fi

# Modify second wallet address
: "${mining_address_second:=false}"
if [ "${mining_address_second}" = "false" ]; then
	if ! sed -i -e "s#^export wallet_address_second=.*\$#export wallet_address_second=\"\"#" alpine-root/etc/local.d/boot.start; then
		echo "Failed to reset the mining percentage of the second wallet address. Please run the script again."
		exit 1
	fi

	if [ -f alpine-root/etc/xmrig-proxy/config.json.second ]; then
		rm alpine-root/etc/xmrig-proxy/config.json.second
	fi
else
	if [ "${system_config}" = "normal" ]; then
		if ! sed -i -e "s#^export wallet_address_second=.*\$#export wallet_address_second=\"${mining_address_second}\"#" alpine-root/etc/local.d/boot.start; then
			echo "Failed to modify the second wallet address. Please run the script again."
			exit 1
		fi
	fi
	if [ "${system_config}" = "server" ]; then
		cp alpine-root/etc/xmrig-proxy/config.json alpine-root/etc/xmrig-proxy/config.json.second
		if ! sed -i -e "s#\"user\": \".*\$#\"user\": \"${mining_address_second}\",#" alpine-root/etc/xmrig-proxy/config.json.second; then
			echo "Failed to modify wallet address. Please run the script again."
			exit 1
		fi
	fi
fi

# Modify second wallet address mining percentage
: "${mining_address_second_percent:=false}"
if [ "${mining_address_second_percent}" = "false" ]; then
	if ! sed -i -e "s#^export donate_percent_second=.*\$#export donate_percent_second=\"\"#" alpine-root/etc/local.d/boot.start; then
		echo "Failed to reset the mining percentage of the second wallet address. Please run the script again."
		exit 1
	fi
	if ! sed -i -e "s#^donate_percent_second=.*\$#donate_percent_second=\"0.0\"#" alpine-root/etc/xmrig-proxy/proxy-donate.sh; then
		echo "Failed to reset the mining percentage of the second wallet address. Please run the script again."
		exit 1
	fi
else
	if [ "${system_config}" = "normal" ]; then
		if ! sed -i -e "s#^export donate_percent_second=.*\$#export donate_percent_second=\"${mining_address_second_percent}\"#" alpine-root/etc/local.d/boot.start; then
			echo "Failed to modify the mining percentage of the second wallet address. Please run the script again."
			exit 1
		fi
	fi
	if [ "${system_config}" = "server" ]; then
		if ! sed -i -e "s#^donate_percent_second=.*\$#donate_percent_second=\"${mining_address_second_percent}\"#" alpine-root/etc/xmrig-proxy/proxy-donate.sh; then
			echo "Failed to modify the mining percentage of the second wallet address. Please run the script again."
			exit 1
		fi
	fi
fi

# Modify crypto currency
if [ -n "${mining_currency}" ]; then
	if ! sed -i -e "s#^export mining_currency=.*\$#export mining_currency=\"${mining_currency}\"#" alpine-root/etc/local.d/boot.start; then
		echo "Failed to modify mining currency type. Please run the script again."
		exit 1
	fi
fi

# Modify pool addresses
if [ -n "${mining_pool1}" ] || [ -n "${mining_proxy1}" ]; then
	if [ "${system_config}" = "normal" ]; then
		if ! sed -i -e "s#^export mining_pool1=.*\$#export mining_pool1=\"$mining_pool1\"#" alpine-root/etc/local.d/boot.start; then
			echo "Failed to modify mining pool address."
			exit 1
		fi
	fi
	if [ "${system_config}" = "client" ]; then
		if ! sed -i -e "s#^export mining_pool1=.*\$#export mining_pool1=\"$mining_proxy1\"#" alpine-root/etc/local.d/boot.start; then
			echo "Failed to modify mining pool address."
			exit 1
		fi
	fi
	if [ "${system_config}" = "client" ]; then
		sed -i -e "s#^export mining_pool1_nicehash=.*\$#export mining_pool1_nicehash=\"y\"#" alpine-root/etc/local.d/boot.start
	else
		sed -i -e "s#^export mining_pool1_nicehash=.*\$#export mining_pool1_nicehash=\"n\"#" alpine-root/etc/local.d/boot.start
	fi
fi

if [ -n "${mining_pool2}" ] || [ -n "${mining_proxy2}" ]; then
	if [ "${system_config}" = "normal" ]; then
		if ! sed -i -e "s#^export mining_pool2=.*\$#export mining_pool2=\"$mining_pool2\"#" alpine-root/etc/local.d/boot.start; then
			echo "Failed to modify mining pool address."
			exit 1
		fi
	fi
	if [ "${system_config}" = "client" ]; then
		if ! sed -i -e "s#^export mining_pool2=.*\$#export mining_pool2=\"$mining_proxy2\"#" alpine-root/etc/local.d/boot.start; then
			echo "Failed to modify mining pool address."
			exit 1
		fi
	fi
	if [ "${system_config}" = "client" ]; then
		sed -i -e "s#^export mining_pool2_nicehash=.*\$#export mining_pool2_nicehash=\"y\"#" alpine-root/etc/local.d/boot.start
	else
		sed -i -e "s#^export mining_pool2_nicehash=.*\$#export mining_pool2_nicehash=\"n\"#" alpine-root/etc/local.d/boot.start
	fi
fi

if [ -n "${mining_pool3}" ] || [ -n "${mining_proxy3}" ]; then
	if [ "${system_config}" = "normal" ]; then
		if ! sed -i -e "s#^export mining_pool3=.*\$#export mining_pool3=\"$mining_pool3\"#" alpine-root/etc/local.d/boot.start; then
			echo "Failed to modify mining pool address."
			exit 1
		fi
	fi
	if [ "${system_config}" = "client" ]; then
		if ! sed -i -e "s#^export mining_pool3=.*\$#export mining_pool3=\"$mining_proxy3\"#" alpine-root/etc/local.d/boot.start; then
			echo "Failed to modify mining pool address."
			exit 1
		fi
	fi
	if [ "${system_config}" = "client" ]; then
		sed -i -e "s#^export mining_pool3_nicehash=.*\$#export mining_pool3_nicehash=\"y\"#" alpine-root/etc/local.d/boot.start
	else
		sed -i -e "s#^export mining_pool3_nicehash=.*\$#export mining_pool3_nicehash=\"n\"#" alpine-root/etc/local.d/boot.start
	fi
fi

# Disable donation to xmr-stak when running in client mode
if [ "${system_config}" = "client" ]; then
	sed -i -e "s#^export donate_percent_xmr_stak=.*\$#export donate_percent_xmr_stak=\"0.0\"    \# Donations for xmr stak project#" alpine-root/etc/local.d/boot.start
else
	sed -i -e "s#^export donate_percent_xmr_stak=.*\$#export donate_percent_xmr_stak=\"0.2\"    \# Donations for xmr stak project#" alpine-root/etc/local.d/boot.start
fi

# Set system config
if [ "${system_config}" = "normal" ]; then
	sed -i -e "s#^export system_config=.*\$#export system_config=\"normal\"#" alpine-root/etc/local.d/boot.start
elif [ "${system_config}" = "client" ]; then
	sed -i -e "s#^export system_config=.*\$#export system_config=\"client\"#" alpine-root/etc/local.d/boot.start
else
	sed -i -e "s#^export system_config=.*\$#export system_config=\"server\"#" alpine-root/etc/local.d/boot.start
fi

# Create LBU archive
cd alpine-root
#tar --owner root --group root -czf ../"${alpine_release}"/localhost.apkovl.tar.gz *
tar --sort=name --mtime="@${SOURCE_DATE_EPOCH}" --owner root --group root -cf ../"${alpine_release}"/localhost.apkovl.tar *
gzip --no-name -c ../"${alpine_release}"/localhost.apkovl.tar > ../"${alpine_release}"/localhost.apkovl.tar.gz
if [ -f ../"${alpine_release}"/localhost.apkovl.tar ]; then
	rm ../"${alpine_release}"/localhost.apkovl.tar
fi
cd ..

# Encrypt LBU archive
if [ -n "${boot_encryption_password}" ]; then
	if ! openssl enc \
		-pass pass:"${boot_encryption_password}" \
		-"${boot_encryption_cipher}" \
		-salt \
		-md md5 \
		-in "${alpine_release}"/localhost.apkovl.tar.gz \
		-out "${alpine_release}"/localhost.apkovl.tar.gz."${boot_encryption_cipher}"; then
			echo "Failed to set encryption password. Please run the script again."
			exit 1
	fi
	if [ -f "${alpine_release}"/localhost.apkovl.tar.gz ]; then
		rm "${alpine_release}"/localhost.apkovl.tar.gz
	fi
elif "${encrypt_archive}"; then
	if ! openssl enc \
		-"${boot_encryption_cipher}" \
		-salt \
		-md md5 \
		-in "${alpine_release}"/localhost.apkovl.tar.gz \
		-out "${alpine_release}"/localhost.apkovl.tar.gz."${boot_encryption_cipher}"; then
			echo "Failed to set encryption password. Please run the script again."
			exit 1
	fi
	if [ -f "${alpine_release}"/localhost.apkovl.tar.gz ]; then
		rm "${alpine_release}"/localhost.apkovl.tar.gz
	fi
fi

# Set date for reproducible iso
find "${alpine_release}" -print0 | xargs -0r touch --no-dereference --date="@${SOURCE_DATE_EPOCH}"

# Create custom iso
xorriso -as mkisofs \
       	-o bootminer.iso \
	-isohybrid-mbr ./"${alpine_release}"/boot/syslinux/isohdpfx.bin \
	-c boot/syslinux/boot.cat \
	-b boot/syslinux/isolinux.bin \
	-no-emul-boot \
	-boot-load-size 4 \
	-boot-info-table \
	./"${alpine_release}"

echo "Checksum of bootminer.iso:"
sha256sum bootminer.iso

echo ""
echo "The BootMiner iso is finished. You can now write bootminer.iso to a USB or CD/DVD."

# IPFS message for reproducible isos
if [ -z "${boot_encryption_password}" ] && ! "${encrypt_archive}"; then
	echo ""
	echo "PROTIP: This bootminer.iso is a reproducible image."
	echo "This means that other people can reproduce and verify this image."
	echo "You can consider to share the image with the community with IPFS (https://ipfs.io)."
        echo "Other people will be able to download and use the iso without"
        echo "going through the steps of creating one."
	echo "Share the IPFS link, the config and the checksum on the"
        echo "BootMiner subreddit (https://www.reddit.com/r/bootminer)."
	echo ""
	echo "Contribute back to the community and share your work. See you there."
	echo ""
fi
