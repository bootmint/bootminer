#!/bin/sh

# Set donation percent for addresses. Remaining percent is for the first address.
# Sum needs to be between 0-100%
donate_percent_second="0.0"
donate_percent_bootmint="0.2"

# Run single instance
[ "${FLOCKER}" != "$0" ] && exec env FLOCKER="$0" flock -n "$0" "$0" "$@" || :

# Mining percent for first address
donate_percent_first=$(awk -v second="${donate_percent_second}" \
	                    -v bootmint="${donate_percent_bootmint}" \
			    'BEGIN { print 100 - second - bootmint }' )

# Check if total mining percentage is between 0-100% 
if awk -v percent="${donate_percent_first}" 'BEGIN { exit (percent < 0) ? 0 : 1}'; then
	echo "Invalid percentage. Total mining percentage is too high. Decrease total mining percentage to 100%."
	exit 1
elif awk -v percent="${donate_percent_first}" 'BEGIN { exit (percent > 100) ? 0 : 1}'; then
	echo "Invalid percentage. Total mining percentage is too high for the first address. Change total mining percentage."
	exit 1
fi

# Mine to first address
if [ -s /etc/xmrig-proxy/config.json ]; then
	cp /etc/xmrig-proxy/config.json config.json
	sleep $(awk -v sleep_percent="${donate_percent_first}" 'BEGIN { print 86400 / 100 * sleep_percent }')
fi

# Mine to bootmint address
if [ -s /etc/xmrig-proxy/config.json.bootmint ]; then
	cp /etc/xmrig-proxy/config.json.bootmint config.json
	sleep $(awk -v sleep_percent="${donate_percent_bootmint}" 'BEGIN { print 86400 / 100 * sleep_percent }')
fi

# Mine to second address
if [ -s /etc/xmrig-proxy/config.json.second ]; then
	cp /etc/xmrig-proxy/config.json.second config.json
	sleep $(awk -v sleep_percent="${donate_percent_second}" 'BEGIN { print 86400 / 100 * sleep_percent }')
fi

# Reset to first address
if [ -s /etc/xmrig-proxy/config.json ]; then
	cp /etc/xmrig-proxy/config.json config.json
fi
