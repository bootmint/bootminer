#!/bin/sh

set -u

# Install packages
apk add build-base util-linux-dev libuv libuv-dev libstdc++ haproxy libressl

# Install xmrig-proxy
/bin/su "${miner_user}" -c "tar zxf /home/${miner_user}/xmrig-proxy-"${xmrig_proxy_release}".tar.gz -C /home/${miner_user}/"
/bin/su "${miner_user}" -c "mkdir ${xmrig_proxy_dir}/build"
cd "${xmrig_proxy_dir}"/build
/bin/su "${miner_user}" -c "cmake .. -DUV_LIBRARY=/usr/lib/libuv.a -DWITH_HTTPD=OFF"
/bin/su "${miner_user}" -c "make"

# Copy xmrig-proxy binary
cp "${xmrig_proxy_dir}"/build/xmrig-proxy /usr/local/bin/
chown root:root /usr/local/bin/xmrig-proxy
chmod 755 /usr/local/bin/xmrig-proxy

# Configure xmrig-proxy
if [ -s /etc/xmrig-proxy/config.json ]; then
	cp /etc/xmrig-proxy/config.json "${config_file_dir}"/config.json
elif [ -s /etc/xmrig-proxy/config.json.second ]; then
	cp /etc/xmrig-proxy/config.json.second "${config_file_dir}"/config.json
elif [ -s /etc/xmrig-proxy/config.json.bootmint ]; then
	cp /etc/xmrig-proxy/config.json.bootmint "${config_file_dir}"/config.json
else
	echo "Installation failed. xmrig-proxy config file is not available."
	exit 1
fi

chown "${miner_user}":"${miner_user}" "${config_file_dir}"/config.json
chmod 640 "${config_file_dir}"/config.json

# SSL keys
if [ ! -s /etc/haproxy/certs/key.pem ]; then
	if [ ! -d /etc/haproxy/certs ]; then
		mkdir -p /etc/haproxy/certs
		chown root:root /etc/haproxy/certs
		chmod 755 /etc/haproxy/certs
	fi

	openssl req -x509 \
		-newkey rsa:2048 \
		-keyout /etc/haproxy/certs/key.pem \
		-out /etc/haproxy/certs/key.pem \
		-days 3650 \
		-sha256 \
		-nodes \
		-subj "/CN=example.com"
fi

chmod 640 /etc/haproxy/certs/*.pem
chown root:root /etc/haproxy/certs/*.pem

# Start xmrig-proxy
cd "${config_file_dir}"
/bin/su "${miner_user}" -c "/usr/local/bin/xmrig-proxy --config=${config_file_dir}/config.json"

# Start haproxy
/etc/init.d/haproxy start

# Configure crontabs
cat >> /etc/crontabs/"${miner_user}" <<-__EOF__
	# Check for xmrig-proxy every 10 minutes
	*/10	*	*	*	*	pgrep xmrig-proxy || /usr/local/bin/xmrig-proxy --config ${config_file_dir}/config.json >> ${config_file_dir}/log.txt 2>&1

	# Check for proxy-donate.sh every 10 minutes
	*/10	*	*	*	*	/etc/xmrig-proxy/proxy-donate.sh >> ${config_file_dir}/log.txt 2>&1
	__EOF__
