# Monero BootMiner 0.8 (alpha)

BootMiner makes it really easy to support and mine [Monero](https://getmonero.org). Just boot it and it will automatically start to mine Monero.  
  
BootMiner runs from USB and CD/DVD and does not require you to install anything on a harddisk. It runs entirely from memory. After a reboot your system will work as before.  
  
Once BootMiner is ready to mine it will automatically eject the disk. You can use the disk to setup other systems.

## Casual and social mining to support the network and the community

BootMiner is mainly aimed at casual and socially minded miners who want to contribute to the Monero network and receive a small reward in return. The reward is likely too small for for-profit miners, but pooling donations to support important initiatives and people in the Monero community may lead to significant improvements in the long run. And that may be invaluable. BootMiner has a list of donation options that will make it easy to donate the mining rewards to important people and projects in the Monero space.  
  
BootMiner comes with a simple setup script that you need to run to create a bootminer iso. The script will present you with a menu that will allow you to select one of the predefined donation options or configure a custom address.  
  
You can create as many bootminer isos as you want. Each can have a different donation address. You can create an iso that mines to the Monero development fund and an iso that mines to your own wallet. You can also create images for friends, family and colleagues. Every additional miner will contribute to the security of the network.  
  
BootMiner only supports CPU mining at the moment. This will hopefully change in the near future.

## Predefined Monero donation options in BootMiner

We hope that you consider to donate to one of the donation addresses listed below. All options will support the development of Monero:  
  
**Monero development fund** - 44AFFq5kSiGBoZ4NMDwYtN18obc8AemS33DBLWs3H7otXft3XjrpDtQGv7SqSsaBYBb98uNbr2VBBEt7f2wfn3RVGQBEP3A  
**Moneromooo (Monero developer)** - 4AfUP827TeRZ1cck3tZThgZbRCEwBrpcJTkA1LCiyFVuMH4b5y59bKMZHGb9y58K3gSjWDCBsB4RkGsGDhsmMG5R2qmbLeW  
**Monero forum funding system (FFS)** - 45ttEikQEZWN1m7VxaVN9rjQkpSdmpGZ82GwUps66neQ1PqbQMno4wMY8F5jiDt2GoHzCtMwa7PDPJUJYb1GYrMP4CwAwNp  
**Monerujo (Android wallet)** - 4AdkPJoxn7JCvAby9szgnt93MSEwdnxdhaASxbTBm6x5dCwmsDep2UYN4FhStDn5i11nsJbpU7oj59ahg8gXb1Mg3viqCuk  
**Cake Wallet (IOS wallet)** - 43gN49UjHNdXDgkcWHTxceHNjXBxcKsReSNThGwzHVavHeZ4SSxSCPT8EpD5cbwAWqEqFQw12rsyTJbKGbeXo43SVpPXZ2W  
**BootMint (BootMiner creators)** - 45YAKAGsns8VZKYuWyzctzSCgd3k1VUqLFre7fKRRvxiaJEftCsQNMYVoddDPjPxB4QSY5BSsSoVB1BomzVopfKgEVT6Cyj  
  
We plan add a few other donation addresses to this list.  
  
Monero research lab?  
Monero outreach?  
Monero bug bounty?  
Kovri?  
...?  

## Monero mining pools

Bootminer will mine to one of the following pools:

- gulf.moneroocean.stream:443
- mine.xmrpool.net:443
- xmrpool.eu:9999

These pools are selected because they are small, have low fees and support TLS. This is just an initial selection. This list may change in the future. You can configure mining pools in the `bootminer.sh` script.

## Features

- Open source
- Based on [Alpine Linux](https://alpinelinux.org)
- [Xmr-stak](https://github.com/fireice-uk/xmr-stak) mining software
- Runs from USB and CD/DVD
- Runs entirely in memory
- Automatically eject BootMiner media after boot. You can boot multiple systems from a single USB or CD/DVD.
- Create custom images for friends and family in a quick and easy way
- Mine other cryptonote currencies (Aeon, etc.)
- Tor support (mining traffic only)
- Reproducible isos (experimental)
- Encrypt sensitive files on the iso (decrypt on boot)
- Mine a percentage to a second address (experimental)
- SSH support (experimental)
- Run custom commands during the setup (experimental)
- Wireless support (experimental)
- Xmrig-proxy server with SSL/TLS support (experimental)
- Mining proxy client support (experimental)

## Dev Fee

BootMiner will donate 0.2% of the hashes to xmr-stak and BootMiner. This will help support the development of both projects. You can change the percentage in `alpine-root/etc/local.d/boot.start`

## TODO

- Add more donation options
- AMD and NVIDIA GPU support?
- PXE boot support?
- Kovri support?
- Fix bugs

## Requirements

- A Linux system to create the BootMiner iso
- A 64 bit Intel or AMD CPU on the miner
- A USB or CD/DVD
- Wired or wireless network connection

## Create a Monero wallet

Use one of these wallets to create a Monero wallet:

- [Desktop (GUI, CLI)](https://getmonero.org)
- [Web wallet & IOS (MYMONERO](https://mymonero.com)
- [Android (MONERUJO)](https://monerujo.io)
- [IOS (CAKE WALLET)](https://cakewallet.io)

Create a backup of the seed and store it at a secure location.

## How to create a BootMiner iso

- Boot a Linux distribution (Ubuntu, Debian, Fedora, etc.)

- Install bsdtar, wget, git and xorriso
```
# Ubuntu and Debian based distributions
sudo apt-get update && sudo apt-get install bsdtar wget git xorriso

# Centos and Fedora based distributions
sudo dnf install bsdtar wget git xorriso
```

- Clone the BootMiner repository (or download bootminer.sh):
```
git clone https://bitbucket.org/bootmint/bootminer.git
```

- Run bootminer.sh script:
```
cd bootminer && ./bootminer.sh
```

- A menu will appear that will allow you to configure a Monero mining address. Select one of the menu options. You need to add a Monero 'mining' address when you select '*CUSTOM & SECOND WALLET ADDRESS*'. A Monero mining address is 95 characters long and starts with a 4. Don't use an address that starts with an 8, that is a sub-address and those are not supported (yet).

- Write the `bootminer.iso` to a USB or CD/DVD. You can use `dd` to write the image to USB stick. Make sure you write to the correct device. If you plan to burn BootMiner to a CD/DVD you will need a CD/DVD burning application and use the `burn image` option.
```
dd if=bootminer.iso of=/dev/USBDISK bs=1M && sync
```

## How to mine to a second 'donation' address

You can use this option to mine 50-99% to your main address and mine the remaining portion to the Monero development fund, the Forum Funding System or another address.

- Run `./bootminer.sh`
- Select '*Custom & Second Wallet Address*'
- Enter the main wallet address in '*Enter primary wallet address*'
- Enter 'y' to '*Would you like to mine a (small) portion to a second address...*'
- Enter the second wallet address in '*Enter the second wallet address*'
- Enter the mining percentage in '*Enter the mining percentage for the second wallet*'. The number needs to be between 1-50.

## How to add a custom Tor config to the BootMiner iso

- Create the torrc.d directory
```
mkdir ./alpine-root/etc/torrc.d
```

- Add config files to the torrc.d directory

- Create a BootMiner iso

## How to enable SSH support

There are two ways to enable SSH support:  
  
1. Add a public key to `alpine-root/home/bootminer/.ssh/authorized_keys` or `alpine-root/root/.ssh/authorized_keys`
2. Create `alpine-root/etc/ssh/sshd_config` and add a valid configuration to the file
  
Option one will automatically configure SSH to require public key authentication. Option two allows custom SSH configurations, but be aware of the default login credentials.  

## How to run custom commands at the end of the setup

Add the commands to: `alpine-root/etc/custom.sh` and create a BootMiner iso.  
  
The commands are executed as root.  

## How to configure wireless networking

- Copy `alpine-root/etc/wpa_supplicant/wpa_supplicant.conf.sample` to `alpine-root/etc/wpa_supplicant/wpa_supplicant.conf`
- Add one or more wireless networks to `alpine-root/etc/wpa_supplicant/wpa_supplicant.conf`
- Create a BootMiner iso

BootMiner will automatically try to connect to the wireless network.

## How to create a xmrig-proxy server iso

- Optional: Configure a static ip address and dns server in `alpine-root/etc/setup-alpine.conf`
- Optional: Add a ssh key to `alpine-root/home/bootminer/.ssh/authorized_keys`
- Run `./bootminer.sh`
- Select option 8 *'mining proxy server'*
- Follow the instructions
- Write the iso to a USB or CD

## How to create a mining proxy client iso

- Optional: Add a ssh key to `alpine-root/home/bootminer/.ssh/authorized_keys`
- Configure the hostname or ip address of a mining proxy server in `bootminer.sh`. Set the address with: *'mining_proxy1'*.
- Run `./bootminer.sh`
- Select option 9 *'mining proxy client'*
- Follow the instructions
- Write the iso to a USB or CD

## How to change the configuration of a running xmrig-proxy server

- Modify the configuration files in `/etc/xmrig-proxy/` (**not** /home/bootminer/config.json)
- Kill xmrig-proxy and proxy-donate.sh (or wait up to 24 hours for the changes to go live)
- Copy /etc/xmrig-proxy/config.json to /home/bootminer/config.json
- Start xmrig-proxy: `xmrig-proxy --config /home/bootminer/config.json` (or wait up to 10 minutes for the cronjob)

## How to use BootMiner

Connect the computer to a wired network. Insert the BootMiner media. Boot the computer from the media. Some systems will do this by default, others will require a specific key combination. Let it run the setup and you're done.  
  
Once you see the login screen you can remove the USB or CD/DVD from the system. The CD/DVD will automatically eject. You can also safely unplug the USB drive at this point.

## Login credentials and mining log

- Normal user account: `bootminer`. Password: `bootminer`.
- Root password: `bootminer`

- Check the log file with the normal user account:
```
tail -f log.txt
```
- If you want to change the default passwords on the iso you need to edit `alpine-root/etc/local.d/boot.start` and run the `bootminer.sh` script.

## Poweroff the system

- Push the powerbutton or login with the root account and type:
```
poweroff
```
- Poweroff the system after a specified time. Login as root and type:
```
poweroff -d SECONDS
```

## PROTIP: Reproducible isos, decentralized file sharing and IPFS

BootMiner supports reproducible isos. This allows everyone to reproduce and verify isos of other people. This in turn makes it easy to distribute and download the isos via a decentralized file sharing platform like [IPFS](https://ipfs.io).  
  
Anyone can now create isos for people and projects that need donations (who doesn't) and make it very easy to download, verify and run them.  
  
We know plenty of high quality projects that are in need of some extra donations, but we can only do so much with the '*predefined donation list*'. With reproducible isos you now have a way to support your favorite projects.  
  
### How to share reproducible isos with the community

- Install [IPFS](https://ipfs.io)
- Add the bootminer.iso to IPFS
- Share the IPFS link, the config and checksum on the BootMiner [subreddit](https://www.reddit.com/r/bootminer)

## License

MIT

## Support BootMiner

Please consider to donate (hashes) to BootMint. We plan to improve BootMiner and release other projects as well.  
  
Monero address:  
`45YAKAGsns8VZKYuWyzctzSCgd3k1VUqLFre7fKRRvxiaJEftCsQNMYVoddDPjPxB4QSY5BSsSoVB1BomzVopfKgEVT6Cyj`
